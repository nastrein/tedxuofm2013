//
//  SpeakersViewController.m
//  TEDx
//
//  Created by Nolan Astrein on 1/28/12.
//  Copyright (c) 2012 University of Michigan. All rights reserved.
//

#import "SpeakersViewController.h"
#import "SpeakerDetailViewController.h"
#import "UIImage+sizer.h"
#import "SVProgressHUD.h"

@implementation SpeakersViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        mainDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        self.navigationItem.backBarButtonItem.enabled = NO;
        //self.navigationItem.backBarButtonItem.style = UIBarButtonSystemItemPlay;
        table.scrollEnabled = NO;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    self.view.backgroundColor = [UIColor blackColor];
    table.backgroundColor = [UIColor blackColor];
    
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeGradient];
    
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    table.separatorColor = [UIColor colorWithRed:255/255 green:43/255 blue:6/255 alpha:1.0];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = UITextAlignmentCenter;
    label.textColor = [UIColor whiteColor]; // change this color
    self.navigationItem.titleView = label;
    label.text = NSLocalizedString(@"Past Speakers", @"");
    [label sizeToFit];
    responseData = [NSMutableData data];
    jsonResponse = [[NSArray alloc]init];
    speakers = [[NSMutableArray alloc]init];
    content = [[NSMutableArray alloc]init];
    pictures = [[NSMutableArray alloc]init];
    picturesURL = [[NSMutableArray alloc]init];
    videoURL = [[NSMutableArray alloc]init];
    tempContent = [[NSString alloc]init];
    tempPicture = [[NSString alloc]init];
    tempSpeaker = [[NSString alloc]init];    
    if(mainDelegate.speakerInfo.speakers.count == 0){
        NSString *url = @"http://tedxuofm.com/api/talks/search/";
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [connection start];
    }else {
    
        speakers = mainDelegate.speakerInfo.speakers;
        content = mainDelegate.speakerInfo.content;
        videoURL = mainDelegate.speakerInfo.videoURL;
        pictures = mainDelegate.speakerInfo.pictures;
        picturesURL = mainDelegate.speakerInfo.picturesURL;
        [SVProgressHUD dismiss];
        self.navigationItem.backBarButtonItem.enabled = YES;
        table.scrollEnabled =YES;
        table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        [table reloadData];
    }
    // Do any additional setup after loading the view from its nib.
}

//- (void)viewDidUnload
//{
//    [super viewDidUnload];
//    speakers = nil;
//    content = nil;
//    pictures = nil;
//    picturesURL = nil;
//    videoURL = nil;
//    responseData = nil;
//    jsonResponse = nil;
//    
//    tempContent = nil;
//    tempPicture = nil;
//    tempSpeaker = nil;
//    
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	noResults.text = @"Connection Failed";
    [SVProgressHUD dismiss];
    self.navigationItem.backBarButtonItem.enabled = YES;
    table.scrollEnabled = YES;

    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Aww Snap You Are Not Connected The Internet!"
                                                      message:nil
                                                     delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    
    [message setAlertViewStyle:UIAlertViewStyleDefault];
    
    [message show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError *error = nil;
    [SVProgressHUD dismiss];
    self.navigationItem.backBarButtonItem.enabled = YES;
    table.scrollEnabled =YES;
    table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    if (responseData == nil) {
        noResults.text = @"No Results";
        return;
    }
    
    jsonResponse = [NSJSONSerialization JSONObjectWithData:responseData
                                                   options:NSJSONReadingAllowFragments
                                                     error:&error];
    
    NSArray *dictionary = [jsonResponse valueForKey:@"talks"];
    
    [dictionary enumerateObjectsWithOptions:NSEnumerationReverse
                                   usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                       
                                       tempSpeaker = [obj objectForKey:@"title"];
                                       [mainDelegate.speakerInfo.speakers addObject:tempSpeaker];
                                       
                                       tempContent = [obj objectForKey:@"content"];
                                       [mainDelegate.speakerInfo.content addObject:tempContent];
                                       UIImage *image = [[UIImage alloc]init];
                                       if([obj objectForKey:@"thumbnail"]) {
                                           
                                           tempPicture = [obj objectForKey:@"thumbnail"];
                                           NSURL *url = [NSURL URLWithString:tempPicture];
                                           [mainDelegate.speakerInfo.picturesURL addObject:tempPicture];
                                           image =[UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
                                           [mainDelegate.speakerInfo.pictures addObject:image];
                                       }
                                       else { 
                                           image = [UIImage imageNamed:@"x.png"];
                                           [mainDelegate.speakerInfo.pictures addObject:image];
                                       }                                       
                                       //NSDictionary *customFields = [obj objectForKey:@"custom_fields"];
                                       //if([obj objectForKey:@"talk_meta_video_id"]){
                                        tempVideo = [obj objectForKey:@"video_id"];
                                       //}
                                       [mainDelegate.speakerInfo.videoURL addObject:tempVideo];
                                       
                                   }];
    speakers = mainDelegate.speakerInfo.speakers;
    content = mainDelegate.speakerInfo.content;
    videoURL = mainDelegate.speakerInfo.videoURL;
    pictures = mainDelegate.speakerInfo.pictures;
    picturesURL = mainDelegate.speakerInfo.picturesURL;
    [table reloadData];
    NSLog(@"%d pics", [mainDelegate.speakerInfo.pictures count]);
    NSLog(@"%d urls", [mainDelegate.speakerInfo.picturesURL count]); 
    NSLog(@"%d ids", [mainDelegate.speakerInfo.videoURL count]); 
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [speakers count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *CellIdentifier = @"Custom";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
	}
    cell.textLabel.text = [speakers objectAtIndex:(speakers.count - indexPath.row) - 1];
    cell.detailTextLabel.text = [content objectAtIndex:(speakers.count - indexPath.row) - 1];
    
    cell.textLabel.numberOfLines = 2;
    cell.detailTextLabel.numberOfLines = 2;
    
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.textColor = [UIColor colorWithRed:255/255 green:43/255 blue:6/255 alpha:1.0];
    
	//cell.textLabel.numberOfLines = ceilf([[speakers objectAtIndex:indexPath.row] sizeWithFont:[UIFont boldSystemFontOfSize:18] constrainedToSize:CGSizeMake(300, MAXFLOAT) lineBreakMode:UILineBreakModeWordWrap].height/20.0);
   // cell.detailTextLabel.numberOfLines = ceilf([[content objectAtIndex:indexPath.row] sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(300, MAXFLOAT) lineBreakMode:UILineBreakModeWordWrap].height/20.0);
    
    cell.textLabel.font = [UIFont systemFontOfSize:18];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
    
    CGSize myRect;
    
    
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [cell.imageView sizeThatFits:myRect]; 
    UIImage *thumb = [pictures objectAtIndex:(speakers.count - indexPath.row) - 1];
    thumb = [thumb makeThumbnailOfSize:CGSizeMake(130.0, 100.0)];
    cell.imageView.image = thumb;
    

	return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *) indexPath {
    NSString *titleString = [speakers objectAtIndex:(speakers.count - indexPath.row) - 1];
	NSString *detailString = [content objectAtIndex:(speakers.count - indexPath.row) - 1];
	CGSize titleSize = [titleString sizeWithFont:[UIFont boldSystemFontOfSize:18] constrainedToSize:CGSizeMake(300, MAXFLOAT) lineBreakMode:UILineBreakModeWordWrap];
	CGSize detailSize = [detailString sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(300, MAXFLOAT) lineBreakMode:UILineBreakModeWordWrap];
	
	return 100;//((detailSize.height+titleSize.height)*1.2);
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [table deselectRowAtIndexPath:indexPath animated:YES];
    
    SpeakerDetailViewController *speakerController = [[SpeakerDetailViewController alloc]initWithNibName:@"SpeakerDetail" bundle:nil];
    speakerController.speakerString = [speakers objectAtIndex:(speakers.count - indexPath.row) - 1]; 
    speakerController.contentString = [content objectAtIndex:(speakers.count - indexPath.row) - 1];
    if([picturesURL count] != 0 && [[picturesURL objectAtIndex:(speakers.count - indexPath.row) - 1] length] != 0){
        speakerController.imageString = [picturesURL objectAtIndex:(speakers.count - indexPath.row) - 1];
    }
    speakerController.videoID = [videoURL objectAtIndex:(speakers.count - indexPath.row) - 1];
    [[self navigationController] pushViewController:speakerController animated:YES];    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
