//
//  SpeakerDetailViewController.m
//  TEDx
//
//  Created by Nolan Astrein on 1/29/12.
//  Copyright (c) 2012 University of Michigan. All rights reserved.
//

#import "SpeakerDetailViewController.h"

@implementation SpeakerDetailViewController

@synthesize speakerString, contentString, imageString, videoID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        speaker = [[UILabel alloc]init];
        content = [[UITextView alloc]init];
        image = [[UIImageView alloc]init];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    speaker.text = speakerString;
    content.text = contentString;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = UITextAlignmentCenter;
    label.textColor = [UIColor whiteColor]; // change this color
    self.navigationItem.titleView = label;
    label.text = NSLocalizedString(speaker.text, @"");
    [label sizeToFit];
    [content setBackgroundColor:[UIColor blackColor]];
    speaker.textColor = [UIColor whiteColor];
    content.textColor = [UIColor colorWithRed:255/255 green:43/255 blue:6/255 alpha:1.0];
    if([imageString length] != 0){
        NSURL *url = [NSURL URLWithString:imageString];
        UIImage *person =[UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
        [image setImage:person];
    }
    else {
        image.image = [UIImage imageNamed:@"x.png"];
    }
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Play"  style: UIBarButtonItemStyleBordered target:self action:@selector(play)];
    webView.hidden = YES;
}

-(void)play {
    webView.hidden = NO;
    //Load web view data
    NSString *strWebsiteUlr = [NSString stringWithFormat:@"http://m.youtube.com/watch?gl=US&hl=en&client=mv-google&v="];
    strWebsiteUlr = [strWebsiteUlr stringByAppendingString:videoID];
    NSURL *videoUrl = [NSURL URLWithString:strWebsiteUlr];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:videoUrl];
    [webView loadRequest:requestObj];
    [webView reload];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Hide"  style: UIBarButtonItemStyleBordered target:self action:@selector(hide)];
}

-(void)hide {
    webView.hidden = YES;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Play"  style: UIBarButtonItemStyleBordered target:self action:@selector(play)];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
