//
//  SpeakerInfo.m
//  TEDx
//
//  Created by Nolan Astrein on 3/4/12.
//  Copyright (c) 2012 University of Michigan. All rights reserved.
//

#import "SpeakerInfo.h"

@implementation SpeakerInfo
@synthesize speakers, content, pictures, picturesURL, videoURL;


- (id) init{
	if(self = [super init]){
		speakers = [[NSMutableArray alloc]init];
        content = [[NSMutableArray alloc]init];
        pictures = [[NSMutableArray alloc]init];
        picturesURL = [[NSMutableArray alloc]init];
        videoURL = [[NSMutableArray alloc]init];
	}
	return self;
}

@end
