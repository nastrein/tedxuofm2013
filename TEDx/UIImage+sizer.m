//
//  UIImage+sizer.m
//  TEDx
//
//  Created by Nolan Astrein on 3/5/12.
//  Copyright (c) 2012 University of Michigan. All rights reserved.
//

#import "UIImage+sizer.h"

@implementation UIImage (sizer) 

- (UIImage *) makeThumbnailOfSize:(CGSize)size;
{
    UIGraphicsBeginImageContext(size);  
    // draw scaled image into thumbnail context
    [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newThumbnail = UIGraphicsGetImageFromCurrentImageContext();        
    // pop the context
    UIGraphicsEndImageContext();
    if(newThumbnail == nil) 
        NSLog(@"could not scale image");
    return newThumbnail;
}


@end
