//
//  SocialFeedViewController.h
//  TEDx
//
//  Created by Nolan Astrein on 1/26/13.
//  Copyright (c) 2013 University of Michigan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialFeedViewController : UIViewController  <UITableViewDelegate, UITableViewDataSource> {
    UITableView *table;

    NSMutableArray *tweetArray;
    NSMutableArray *timeArray;
    NSMutableArray *userArray;
    NSMutableArray *imageArray;
    
    NSMutableData *responseData;
    NSArray *jsonResponse;
}

@end
