//
//  LiveDetailViewController.m
//  TEDx
//
//  Created by Nolan Astrein on 3/7/12.
//  Copyright (c) 2012 University of Michigan. All rights reserved.
//

#import "LiveDetailViewController.h"

@implementation LiveDetailViewController
@synthesize speakerString, contentString, imageString;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    speaker.text = speakerString;
    content.text = contentString;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = UITextAlignmentCenter;
    label.textColor = [UIColor whiteColor]; // change this color
    self.navigationItem.titleView = label;
    label.text = NSLocalizedString(speaker.text, @"");
    [label sizeToFit];
    [content setBackgroundColor:[UIColor blackColor]];
    speaker.textColor = [UIColor whiteColor];
    content.textColor = [UIColor colorWithRed:255/255 green:43/255 blue:6/255 alpha:1.0];
    if([imageString length] != 0){
        NSURL *url = [NSURL URLWithString:imageString];
        UIImage *person =[UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
        [image setImage:person];
    }
    else {
        image.image = [UIImage imageNamed:@"x.png"];
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
