//
//  LiveDetailViewController.h
//  TEDx
//
//  Created by Nolan Astrein on 3/7/12.
//  Copyright (c) 2012 University of Michigan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveDetailViewController : UIViewController {
    IBOutlet UILabel *speaker;
    IBOutlet UITextView *content;
    IBOutlet UIImageView *image;
}

@property (nonatomic, retain)  NSString *speakerString;
@property (nonatomic, retain)  NSString *contentString;
@property (nonatomic, retain)  NSString *imageString;

@end
