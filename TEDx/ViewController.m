//
//  ViewController.m
//  TEDx
//
//  Created by Nolan Astrein on 1/28/12.
//  Copyright (c) 2012 University of Michigan. All rights reserved.
//

#import "ViewController.h"
#import "LiveSpeakersViewController.h"
#import "RootViewController.h"
#import "SpeakersViewController.h"
#import "InformationViewController.h"
#import "SocialFeedViewController.h"
#import "TumblrViewController.h"
#import "UIViewController+JTRevealSidebarV2.h"
#import "UINavigationItem+JTRevealSidebarV2.h"
#import "SidebarViewController.h"
#import "JTRevealSidebarV2Delegate.h"

@interface ViewController (Private) <UITableViewDataSource, UITableViewDelegate, SidebarViewControllerDelegate>
@end

@implementation ViewController

@synthesize leftSidebarViewController;
@synthesize leftSelectedIndexPath;

@synthesize timeSlots, speakers;

- (id)init {
    self = [super init];
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:255/255 green:43/255 blue:6/255 alpha:1.0]];
//    UIImage *image = [UIImage imageNamed:@"NavBar.png"];
//    [[UINavigationBar appearance] setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    
    //table.separatorColor = [UIColor colorWithRed:255/255 green:43/255 blue:6/255 alpha:1.0];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"schedule"
													 ofType:@"plist"];
	//---load the list into the dictionary---
	NSDictionary *dic = [[NSDictionary alloc] initWithContentsOfFile:path];
	
	//---save the dictionary object to the property---
	self.speakers = dic;
	
	//---get all the keys in the dictionary object and sort them---
	NSArray *array = [[self.speakers allKeys]
					  sortedArrayUsingSelector:@selector(compare:)];
	
	//---save the keys to the timeSlots property---
	self.timeSlots = array;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = UITextAlignmentCenter;
    label.textColor = [UIColor whiteColor]; // change this color
    self.navigationItem.titleView = label;
    label.text = NSLocalizedString(@"Schedule", @"");
    [label sizeToFit];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:255/255 green:43/255 blue:6/255 alpha:1.0];
    table.separatorColor = [UIColor colorWithRed:255/255 green:43/255 blue:6/255 alpha:1.0];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ButtonMenu.png"]  style:UIBarButtonItemStyleBordered target:self action:@selector(revealLeftSidebar:)];
    
    self.navigationItem.revealSidebarDelegate = self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return [self.timeSlots count];
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
	//---check the current slot based on the section index---
	NSString *slot = [self.timeSlots objectAtIndex:section];
	
	//---returns the speakers in that slot as an array---
	NSArray *speakerSection = [self.speakers objectForKey:slot];
	
	//---return the number of speakers for that timeSlot as the number of rows in that section
	return [speakerSection count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
		 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if(cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
									   reuseIdentifier:CellIdentifier];
	}
	
	// Configure the cell.
	//---get the timeSlot---
	NSString *slot = [self.timeSlots objectAtIndex:[indexPath section]];
	
	//---get the list of speakers for that slot---
	NSArray *speakerSection = [self.speakers objectForKey:slot];
	
	//---get the particular speaker based on that row---
	cell.textLabel.text = [speakerSection objectAtIndex:[indexPath row]];
	cell.textLabel.textColor = [UIColor whiteColor];
	cell.textLabel.numberOfLines = 2;
	[cell setSelectionStyle:0];
	
	return cell;
}

- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section {
	//---get the timeSlot as the section header---
	NSString *slot = [self.timeSlots objectAtIndex:section];
	return slot;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 56;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 0)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 55)];
	label.text = [self.timeSlots objectAtIndex:section];
	label.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
	label.textAlignment = UITextAlignmentCenter;
	label.textColor = [UIColor blackColor];
	label.numberOfLines = 2;
	//UIImage *gradient = [UIImage imageNamed:@"textGradient.png"];
	label.backgroundColor = [UIColor colorWithRed:255/255 green:43/255 blue:6/255 alpha:1.0];
    [headerView addSubview:label];
	return headerView;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

#if EXPERIEMENTAL_ORIENTATION_SUPPORT

// Doesn't support rotating to other orientation at this moment
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    _containerOrigin = self.navigationController.view.frame.origin;
}


- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (self.navigationController.revealedState != JTRevealedStateNo) {
        self.navigationController.view.hidden = YES;
        self.navigationController.view.alpha  = 0;
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    self.navigationController.view.frame = (CGRect){_containerOrigin, self.navigationController.view.frame.size};
    self.navigationController.view.hidden = NO;
    self.navigationController.view.alpha  = 1;
}

- (NSString *)description {
    NSString *logMessage = [NSString stringWithFormat:@"ViewController {"];
    logMessage = [logMessage stringByAppendingFormat:@"\n\t%@", self.view];
    logMessage = [logMessage stringByAppendingFormat:@"\n\t%@", self.navigationController.view];
    logMessage = [logMessage stringByAppendingFormat:@"\n\t%@", self.leftSidebarViewController.view];
    logMessage = [logMessage stringByAppendingFormat:@"\n\t%@", self.rightSidebarView];
    logMessage = [logMessage stringByAppendingFormat:@"\n}"];
    return logMessage;
}

#endif

#pragma mark Action

- (void)revealLeftSidebar:(id)sender {
    JTRevealedState state = JTRevealedStateLeft;
    if (self.navigationController.revealedState == JTRevealedStateLeft) {
        state = JTRevealedStateNo;
    }
    [self.navigationController setRevealedState:state];
}


#pragma mark JTRevealSidebarDelegate

// This is an examle to configure your sidebar view through a custom UIViewController
- (UIView *)viewForLeftSidebar {
    CGRect mainFrame = [[UIScreen mainScreen] applicationFrame];
    UITableViewController *controller = self.leftSidebarViewController;
    if ( ! controller) {
        self.leftSidebarViewController = [[SidebarViewController alloc] init];
        self.leftSidebarViewController.sidebarDelegate = self;
        self.leftSidebarViewController.view.backgroundColor = [UIColor blackColor];
        controller = self.leftSidebarViewController;
        controller.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        controller.tableView.separatorColor = [UIColor colorWithRed:255/255 green:43/255 blue:6/255 alpha:1.0];
        controller.view.frame = CGRectMake(0, mainFrame.origin.y, 270, mainFrame.size.height);
        controller.title = @"TEDxUofM Menu";
        controller.view.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
    }
    return controller.view;
}

@end


@implementation ViewController (Private)

#pragma mark SidebarViewControllerDelegate

- (void)sidebarViewController:(SidebarViewController *)sidebarViewController didSelectObject:(NSObject *)object atIndexPath:(NSIndexPath *)indexPath {
    [self.navigationController setRevealedState:JTRevealedStateNo];
    if (indexPath.row == 0){
        
    }
    else if (indexPath.row == 1){
        LiveSpeakersViewController *controller = [[LiveSpeakersViewController alloc]initWithNibName:@"LiveSpeakersViewController" bundle:nil];
        [[self navigationController] pushViewController:controller animated:YES];
    }
    else if (indexPath.row == 2){
        SpeakersViewController *controller = [[SpeakersViewController alloc]initWithNibName:@"Speakers" bundle:nil];
        controller.view.backgroundColor = [UIColor viewFlipsideBackgroundColor];
        [[self navigationController] pushViewController:controller animated:YES];
    }
    else if (indexPath.row == 3){
        SocialFeedViewController *controller = [[SocialFeedViewController alloc]init];
        [[self navigationController] pushViewController:controller animated:YES];
    }
    else if (indexPath.row == 4){
        TumblrViewController *controller = [[TumblrViewController alloc]init];
        [[self navigationController] pushViewController:controller animated:YES];
    }
    else if (indexPath.row == 5){
        RootViewController *controller = [[RootViewController alloc]initWithNibName:@"RootViewController" bundle:nil];
        [[self navigationController] pushViewController:controller animated:YES];
    }
    else if (indexPath.row == 6){
        InformationViewController *controller = [[InformationViewController alloc]initWithNibName:@"InformationViewController" bundle:nil];
        [[self navigationController] pushViewController:controller animated:YES];
    }
}

- (NSIndexPath *)lastSelectedIndexPathForSidebarViewController:(SidebarViewController *)sidebarViewController {
    return self.leftSelectedIndexPath;
}

@end