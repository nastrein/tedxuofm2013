//
//  LiveInfo.m
//  TEDx
//
//  Created by Nolan Astrein on 3/6/12.
//  Copyright (c) 2012 University of Michigan. All rights reserved.
//

#import "LiveInfo.h"

@implementation LiveInfo
@synthesize pictures, picturesURL, content, speakers;

- (id) init{
	if(self = [super init]){
		speakers = [[NSMutableArray alloc]init];
        content = [[NSMutableArray alloc]init];
        pictures = [[NSMutableArray alloc]init];
        picturesURL = [[NSMutableArray alloc]init];
	}
	return self;
}

@end
