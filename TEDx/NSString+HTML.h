//
//  NSString+HTML.h
//  TEDx
//
//  Created by Nolan Astrein on 1/28/13.
//  Copyright (c) 2013 University of Michigan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (HTML)

@end
