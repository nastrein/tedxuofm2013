//
//  AppDelegate.h
//  TEDx
//
//  Created by Nolan Astrein on 1/28/12.
//  Copyright (c) 2012 University of Michigan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SpeakerInfo.h"
#import "LiveInfo.h"

@class InformationViewController;
@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) UINavigationController *navController;
@property (strong, nonatomic) ViewController *home;
@property (strong, nonatomic) InformationViewController *info;

@property (nonatomic, retain) SpeakerInfo *speakerInfo;
@property (nonatomic, retain) LiveInfo *liveInfo;

@end

