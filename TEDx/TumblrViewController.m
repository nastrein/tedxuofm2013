//
//  TumblrViewController.m
//  TEDx
//
//  Created by Nolan Astrein on 2/8/13.
//  Copyright (c) 2013 University of Michigan. All rights reserved.
//

#import "TumblrViewController.h"

static const NSString * kTumblrURL = @"http://news.tedxuofm.com/";

@interface TumblrViewController () <UIWebViewDelegate>

@property (strong, nonatomic) UIWebView * tumblrWebView;

@end

@implementation TumblrViewController

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = UITextAlignmentCenter;
    label.textColor = [UIColor whiteColor]; // change this color
    self.navigationItem.titleView = label;
    label.text = NSLocalizedString(@"Tumblr", @"");
    [label sizeToFit];
    
    self.tumblrWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    self.tumblrWebView.delegate = self;
    self.tumblrWebView.scalesPageToFit = YES;
    
    [self.view addSubview:self.tumblrWebView];
    
     [self.tumblrWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:(NSString *)kTumblrURL]]];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
