//
//  InformationViewController.m
//  TEDx
//
//  Created by Nolan Astrein on 3/4/12.
//  Copyright (c) 2012 University of Michigan. All rights reserved.
//

#import "InformationViewController.h"

@implementation InformationViewController
@synthesize scrollView;
@synthesize pageControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.view.backgroundColor = [UIColor blackColor];
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)loadView {
    [super loadView];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = UITextAlignmentCenter;
    label.textColor = [UIColor whiteColor]; // change this color
    self.navigationItem.titleView = label;
    label.text = NSLocalizedString(@"TED Info", @"");
    [label sizeToFit];
    [self setupPage];
//    self.view.backgroundColor = [UIColor redColor];
//    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    scroll.pagingEnabled = YES;
//    NSInteger numberOfViews = 3;
//    for (int i = 0; i < numberOfViews; i++) {
//        CGFloat yOrigin = i * self.view.frame.size.width;
//        UIView *awesomeView = [[UIView alloc] initWithFrame:CGRectMake(yOrigin, 0, self.view.frame.size.width, self.view.frame.size.height)];
//        awesomeView.backgroundColor = [UIColor colorWithRed:0.5*i green:0.5 blue:0.5 alpha:1];
//        [scroll addSubview:awesomeView];
//    }
//    scroll.contentSize = CGSizeMake(self.view.frame.size.width * numberOfViews, self.view.frame.size.height);
//    [self.view addSubview:scroll];
    
    
}

#pragma mark -
#pragma mark The Guts
- (void)setupPage
{
	scrollView.delegate = self;
    
	[self.scrollView setBackgroundColor:[UIColor blackColor]];
	[scrollView setCanCancelContentTouches:NO];
	
	scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	scrollView.clipsToBounds = YES;
	scrollView.scrollEnabled = YES;
	scrollView.pagingEnabled = YES;
	
	NSUInteger ninfos = 0;
	CGFloat cx = 0;
	for (; ; ninfos++) {
		NSString *textName = [NSString stringWithFormat:@"info%d", (ninfos + 1)];
        NSString *path = [[NSBundle mainBundle] pathForResource:textName
                                                         ofType:@"plist"];
        NSDictionary *dic = [[NSDictionary alloc] initWithContentsOfFile:path];
        UIFont *font = [UIFont systemFontOfSize:20.0];
        
        NSString *infoText = [dic objectForKey:@"info"];

        if (infoText == nil) {
			break;
		}
        
		UITextView *info = [[UITextView alloc] init];
        info.text = infoText;
        info.textColor = [UIColor whiteColor];
        info.font = font;
        info.backgroundColor = [UIColor blackColor];
        
		CGRect rect = info.frame;
		rect.size.height = 480;
		rect.size.width = 320;
		rect.origin.x = 0 + 320*ninfos;
		rect.origin.y = 0;
        
		info.frame = rect;
        info.editable = NO;
        [info setContentInset:UIEdgeInsetsMake(0,0,105,0)];
		[scrollView addSubview:info];
        
        if(rect.origin.x == 0){
            UIButton *facebook = [[UIButton alloc]init];
            [facebook addTarget:self 
                         action:@selector(goToFacebook)
               forControlEvents:UIControlEventTouchDown];
            UIImage *image  = [UIImage imageNamed:@"facebook_icon.png"];
            [facebook setImage:image forState:UIControlStateNormal];
            facebook.frame = CGRectMake(70.0f, 185.0f, 80.0f, 80.0f);

            [scrollView addSubview:facebook];
            
            UIButton *twitter = [[UIButton alloc]init];
            [twitter addTarget:self 
                         action:@selector(goToTwitter)
               forControlEvents:UIControlEventTouchDown];
            UIImage *image2  = [UIImage imageNamed:@"twitter_icon.png"];
            [twitter setImage:image2 forState:UIControlStateNormal];
            twitter.frame = CGRectMake(70.0f + 80.0f + 20.0f, 185.0f, 78.0f, 78.0f);
            
            [scrollView addSubview:twitter];
            
            UIButton *map = [[UIButton alloc]init];
            [map addTarget:self 
                        action:@selector(goToMap)
              forControlEvents:UIControlEventTouchDown];
            UIImage *image3  = [UIImage imageNamed:@"googlemapsicon.png"];
            [map setImage:image3 forState:UIControlStateNormal];
            map.frame = CGRectMake(115.0f, 60.0f, 90.0f, 90.0f);
            
            [scrollView addSubview:map];
            
            UIButton *arrowButton = [[UIButton alloc]init];
            [arrowButton addTarget:self 
                    action:@selector(goToNothing)
          forControlEvents:UIControlEventTouchDown];
            arrowButton.enabled = NO;
            UIImage *arrow = [[UIImage alloc]init];
            arrow = [UIImage imageNamed:@"arrow.png"];
            [arrowButton setImage:arrow forState:UIControlStateNormal];
            arrowButton.frame = CGRectMake(220.0f, 300.0f, 65.0f, 65.0f);
            [scrollView addSubview:arrowButton];
        }
        
		cx += scrollView.frame.size.width;
	}
	
	self.pageControl.numberOfPages = ninfos;
	[scrollView setContentSize:CGSizeMake(cx, [scrollView bounds].size.height)];
}

-(void)goToFacebook {
    NSURL *fanPageURL = [NSURL URLWithString:@"fb://profile/260422438196"];
    if (![[UIApplication sharedApplication] openURL: fanPageURL]) {
        NSURL *webURL = [NSURL URLWithString:@"http://www.facebook.com/pages/TEDxUofM/260422438196"];
        [[UIApplication sharedApplication] openURL: webURL];
    }
}

-(void)goToTwitter {
    NSURL *fanPageURL = [NSURL URLWithString:@"twitter://user?screen_name=TEDXUofM"];
    if (![[UIApplication sharedApplication] openURL: fanPageURL]) {
        NSURL *webURL = [NSURL URLWithString:@"http://twitter.com/#!/TEDXUofM"];
        [[UIApplication sharedApplication] openURL: webURL];
    }
}

-(void)goToMap {
    NSString *title = @"U%20of%20M%20Power%20Center";
    float latitude = 42.2807698;
    float longitude = -83.7359262;
    int zoom = 13;
    NSString *stringURL = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%@@%1.6f,%1.6f&z=%d", title, latitude, longitude, zoom];
    NSURL *url = [NSURL URLWithString:stringURL];
    [[UIApplication sharedApplication] openURL:url];
}

#pragma mark -
#pragma mark UIScrollViewDelegate stuff
- (void)scrollViewDidScroll:(UIScrollView *)_scrollView
{
    if (pageControlIsChangingPage) {
        return;
    }
    
	/*
	 *	We switch page at 50% across
	 */
    CGFloat pageWidth = _scrollView.frame.size.width;
    int page = floor((_scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = page;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)_scrollView 
{
    pageControlIsChangingPage = NO;
}

#pragma mark -
#pragma mark PageControl stuff
- (IBAction)changePage:(id)sender 
{
	/*
	 *	Change the scroll view
	 */
    CGRect frame = scrollView.frame;
    frame.origin.x = frame.size.width * pageControl.currentPage;
    frame.origin.y = 0;
	
    [scrollView scrollRectToVisible:frame animated:YES];
    
	/*
	 *	When the animated scrolling finishings, scrollViewDidEndDecelerating will turn this off
	 */
    pageControlIsChangingPage = YES;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
