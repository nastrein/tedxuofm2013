//
//  SpeakerDetailViewController.h
//  TEDx
//
//  Created by Nolan Astrein on 1/29/12.
//  Copyright (c) 2012 University of Michigan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpeakerDetailViewController : UIViewController {
    IBOutlet UILabel *speaker;
    IBOutlet UITextView *content;
    IBOutlet UIImageView *image;
    IBOutlet UIWebView *webView;
}

@property (nonatomic, retain)  NSString *speakerString;
@property (nonatomic, retain)  NSString *contentString;
@property (nonatomic, retain)  NSString *imageString;
@property (nonatomic, retain)  NSString *videoID;

-(void)play;
-(void)hide;


@end
