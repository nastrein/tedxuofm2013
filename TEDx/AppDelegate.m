//
//  AppDelegate.m
//  TEDx
//
//  Created by Nolan Astrein on 1/28/12.
//  Copyright (c) 2012 University of Michigan. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"
#import "InformationViewController.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize home, navController, speakerInfo, liveInfo, info;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    
    if ([def valueForKey:HAS_REGISTERED_KEY])
        home = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    else {
        [[[UIAlertView alloc] initWithTitle:@"TED Tour" message:@"Would you like to take a tour of TED?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil] show];
    }
    
     home = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    self.navController = [[UINavigationController alloc]initWithRootViewController:home];
    self.window.rootViewController = self.navController;
    self.speakerInfo = [[SpeakerInfo alloc]init];
    self.liveInfo = [[LiveInfo alloc]init];
    //UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    
#if EXPERIEMENTAL_ORIENTATION_SUPPORT
    UINavigationController *container = [[UINavigationController alloc] init];
    [container setNavigationBarHidden:YES animated:NO];
    [container setViewControllers:[NSArray arrayWithObject:navController] animated:NO];
    self.window.rootViewController = container;
#else
    self.window.rootViewController = navController;
#endif
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex==1) {
        info = [[InformationViewController alloc] initWithNibName:@"InformationViewController" bundle:nil];
        [self.navController pushViewController:info animated:YES];
    }
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setValue:@"YES" forKey:HAS_REGISTERED_KEY];
    [def synchronize];
}

@end
