//
//  SocialFeedViewController.m
//  TEDx
//
//  Created by Nolan Astrein on 1/26/13.
//  Copyright (c) 2013 University of Michigan. All rights reserved.
//

#import "SocialFeedViewController.h"
#import "SVProgressHUD.h"
#import "NSDate+TimeAgo.h"

@interface SocialFeedViewController () <NSURLConnectionDelegate>

@end

@implementation SocialFeedViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor blackColor]];
        
        table = [[UITableView alloc] initWithFrame:CGRectMake(0., 0., self.view.frame.size.width, self.view.frame.size.height- 44.)];
        [table setDataSource:self];
        [table setDelegate:self];
        [table setBackgroundColor:[UIColor blackColor]];
        table.separatorStyle = UITableViewCellSeparatorStyleNone;
        table.separatorColor = [UIColor blackColor];
        [self.view addSubview:table];
        
        tweetArray = [[NSMutableArray alloc] init];
        timeArray = [[NSMutableArray alloc] init];
        userArray = [[NSMutableArray alloc] init];
        imageArray = [[NSMutableArray alloc] init];
        
        jsonResponse = [[NSArray alloc] init];
        responseData = [NSMutableData data];
        
        UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc]
                                   initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                   target:self
                                   action:@selector(refresh)];
        self.navigationItem.rightBarButtonItem = refreshButton;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont boldSystemFontOfSize:20.0];
        label.textAlignment = UITextAlignmentCenter;
        label.textColor = [UIColor whiteColor]; // change this color
        self.navigationItem.titleView = label;
        label.text = NSLocalizedString(@"Social Feed", @"");
        [label sizeToFit];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self refresh];
}

- (void)refresh {
    
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeGradient];
    
    [timeArray removeAllObjects];
    [tweetArray removeAllObjects];
    [imageArray removeAllObjects];
    
    NSString *url = @"http://search.twitter.com/search.json?q=tedxuofm";
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [tweetArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *CellIdentifier = @"Custom";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle  reuseIdentifier:CellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.textColor = [UIColor lightTextColor];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
    
    if([tweetArray count] && [timeArray count] && [imageArray count]) {
        cell.textLabel.numberOfLines = ceilf([[tweetArray objectAtIndex:indexPath.row] sizeWithFont:[UIFont boldSystemFontOfSize:14] constrainedToSize:CGSizeMake(260, MAXFLOAT) lineBreakMode:UILineBreakModeCharacterWrap].height/14.0);
        cell.detailTextLabel.numberOfLines = ceilf([[timeArray objectAtIndex:indexPath.row] sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(260, MAXFLOAT) lineBreakMode:UILineBreakModeCharacterWrap].height/12.0);
        
        // Configure the cell
        cell.textLabel.text = [tweetArray objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [timeArray objectAtIndex:indexPath.row];
        cell.imageView.image = [imageArray objectAtIndex:indexPath.row];
    }
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *titleString  = [[NSString alloc] init];
    NSString *detailString = [[NSString alloc] init];
    if([tweetArray count] && [timeArray count] && [imageArray count]) {
        titleString = [tweetArray objectAtIndex:indexPath.row];        
        detailString = [timeArray objectAtIndex:indexPath.row];
    }
    
    CGSize detailSize = [detailString sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(260, MAXFLOAT) lineBreakMode:UILineBreakModeWordWrap];
    CGSize titleSize = [titleString sizeWithFont:[UIFont boldSystemFontOfSize:14] constrainedToSize:CGSizeMake(260, MAXFLOAT) lineBreakMode:UILineBreakModeWordWrap];
    
    CGFloat height = MAX((titleSize.height + detailSize.height + 20.f), 44.0f);

	return height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - NSURLConnectionDelegate methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [SVProgressHUD dismiss];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aww Snap"
														message:[NSString stringWithFormat:@"You are not connected to the internet"]
													   delegate:self cancelButtonTitle:@"Ok"
											  otherButtonTitles:nil];
	[alertView show];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [SVProgressHUD dismiss];
    table.separatorColor = [UIColor redColor];
    table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    NSError *error = nil;
    if (responseData == nil) {
        return;
    }
    
    jsonResponse = [NSJSONSerialization JSONObjectWithData:responseData
                                                   options:NSJSONReadingAllowFragments
                                                     error:&error];
    
    NSArray * temp = [[(NSDictionary *)jsonResponse allValues] objectAtIndex:3]; /* fix me */
        
    [temp enumerateObjectsWithOptions:NSEnumerationConcurrent
                           usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                NSString *time = [obj objectForKey:@"created_at"];
                                NSDate *dateCreated = [self convertTwitterDateToNSDate:time];
                                time = [dateCreated timeAgo];
                               
                                [timeArray addObject:time];

                                NSString *tweet = [obj objectForKey:@"text"];

                                NSString * user = [NSString stringWithFormat:@"@%@:\n",[obj objectForKey:@"from_user"]];

                                NSURL *url = [NSURL URLWithString:[obj objectForKey:@"profile_image_url"]];
                                NSData *data = [[NSData alloc] initWithContentsOfURL:url];
                                UIImage * pic = [UIImage imageWithData:data];
                                [imageArray addObject:pic];

                                tweet = [user stringByAppendingString:tweet];
                                [tweet stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
                                [tweetArray addObject:tweet];    
                           }];
    
    [table scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    [table reloadData];
}

- (NSDate*)convertTwitterDateToNSDate:(NSString*)created_at
{
    static NSDateFormatter* df = nil;
    
    df = [[NSDateFormatter alloc] init];
    [df setTimeStyle:NSDateFormatterFullStyle];
    [df setFormatterBehavior:NSDateFormatterBehavior10_4];
    [df setDateFormat:@"EEE, d LLL yyyy HH:mm:ss Z"];
    
    NSDate *convertedDate = [df dateFromString:created_at];
    
    return convertedDate;
}

@end
