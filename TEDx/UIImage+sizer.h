//
//  UIImage+sizer.h
//  TEDx
//
//  Created by Nolan Astrein on 3/5/12.
//  Copyright (c) 2012 University of Michigan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (sizer)
- (UIImage *) makeThumbnailOfSize:(CGSize)size;
@end
