//
//  SpeakersViewController.h
//  TEDx
//
//  Created by Nolan Astrein on 1/28/12.
//  Copyright (c) 2012 University of Michigan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface SpeakersViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView *table;
    NSMutableData *responseData;
    NSMutableArray *speakers;
    NSMutableArray *content;
    NSMutableArray *pictures;
    NSMutableArray *picturesURL;
    NSMutableArray *videoURL;
    NSArray *jsonResponse;
    IBOutlet UILabel *noResults;
    
    AppDelegate *mainDelegate;
    __block NSString *tempSpeaker;
    __block NSString *tempContent;
    __block NSString *tempPicture;
    __block NSString *tempVideo;
}



@end
