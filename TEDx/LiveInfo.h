//
//  LiveInfo.h
//  TEDx
//
//  Created by Nolan Astrein on 3/6/12.
//  Copyright (c) 2012 University of Michigan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LiveInfo : NSObject {
    
}

@property (nonatomic, retain) NSMutableArray *speakers;
@property (nonatomic, retain) NSMutableArray *content;
@property (nonatomic, retain) NSMutableArray *pictures;
@property (nonatomic, retain) NSMutableArray *picturesURL;

@end
